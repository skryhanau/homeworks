<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output indent="yes" />
	<xsl:key name="keyName" match="*|@*" use="name()" />

	<xsl:template match="/">
		<xsl:for-each select="//*[generate-id(.) = generate-id(key('keyName', name()))]|//@*[generate-id(.) = generate-id(key('keyName', name()))]">
			Node <xsl:value-of select="name()" /> 
			found <xsl:value-of select="count(key('keyName', name()))" /> 
			times. &#xa;
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>