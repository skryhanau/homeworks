<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output indent="yes" />
	<xsl:key name="keysName" match="node()[@*]" use="substring(@*, 1, 1)" />

	<xsl:template match="/">
		<xsl:apply-templates select="//node()[generate-id(.) = generate-id(key('keysName', substring(@*, 1, 1)))]">
			<xsl:sort select="substring(@*, 1, 1)" />
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="node()">
		<capital value="{substring(@*, 1, 1)}">​
			<xsl:for-each select="key('keysName', substring(@*, 1, 1))">
				<xsl:element name="{name(@*)}">
					<xsl:value-of select="@*" />
				</xsl:element>
			</xsl:for-each>
		</capital>
	</xsl:template>
</xsl:stylesheet>