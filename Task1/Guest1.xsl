<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" version="1.0" xmlns:v="xml_version_1.0" 
    xmlns:end="https://www.meme-arsenal.com/create/template/43024"
    exclude-result-prefixes="v">

    <xsl:template match="/">
        <xsl:for-each select="v:Guests/v:Guest[@Name='Jimmy']/preceding-sibling::*">
            <xsl:variable name="Upper">ABCDEFGHJIKLMNOPQRSTVXYZ</xsl:variable>
            <xsl:variable name="Lower">abcdefghjiklmnopqrstvxyz</xsl:variable>
            <xsl:value-of select="translate(@Name, $Upper, $Lower)"/>
            <xsl:text>&#10;</xsl:text>
        </xsl:for-each>
___________________________________________

<xsl:for-each select="v:Guests/v:Guest[@Name='Jimmy']/following-sibling::*">
            <xsl:if test="@Nationalty!='BY'">
                <xsl:value-of select="v:Profile/v:Address"/>
                <xsl:text>&#10;</xsl:text>
            </xsl:if>
</xsl:for-each>
___________________________________________
        <xsl:apply-templates select="//v:Address"></xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="//v:Address">
        
        <xsl:if test="not(contains(node(), 'Pushkinskaya'))">
            <xsl:copy>
                <xsl:attribute name="Name">
                    <xsl:value-of select="../../@Name"/>
                </xsl:attribute>
                <xsl:attribute name="Nationalty">
                    <xsl:value-of select="../../@Nationalty"/>
                </xsl:attribute>
                <xsl:apply-templates select="node()"/>
            </xsl:copy>
        </xsl:if>
        
    </xsl:template>

</xsl:stylesheet>