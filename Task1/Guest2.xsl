<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xmlns:v="xml_version_1.0" xmlns:end="https://www.meme-arsenal.com/create/template/43024"
    extension-element-prefixes="v" version="1.0">
    
    <xsl:template match="/">
        <xsl:for-each select="v:Guests/child::*">
            <xsl:value-of select="v:Type"/>|
            <xsl:value-of select="@Age"/>|
            <xsl:value-of select="@Nationalty"/>|
            <xsl:value-of select="@Gender"/>|
            <xsl:value-of select="@Name"/>|
            <xsl:value-of select="v:Profile/v:Address"/>|
            <xsl:if test="v:Profile/v:Email">
                <xsl:value-of select="v:Profile/v:Email"/>
            </xsl:if>#
        </xsl:for-each>
        
    </xsl:template>
    
    
</xsl:stylesheet>