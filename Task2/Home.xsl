<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" xmlns:h="HouseChema" xmlns:i="HouseInfo" xmlns:r="RoomsChema"
    version="1.0">
    
    <xsl:template match="/">
        <AllRoom>
            <xsl:apply-templates select="//r:Room">
                <xsl:sort select="ancestor::h:House/@City"/>
                <xsl:sort select="ancestor::h:House/i:Address"/>
                <xsl:sort select="ancestor::h:Block/@number"/>
                <xsl:sort select="@nuber"/>
            </xsl:apply-templates>
        </AllRoom>
    </xsl:template>
    <xsl:template match="r:Room">

        <Room> 
            <Adress>
                <xsl:variable name="Upper">ABCDEFGHJIKLMNOPQRSTVXYZ</xsl:variable>
                <xsl:variable name="Lower">abcdefghjiklmnopqrstvxyz</xsl:variable>
                <xsl:value-of select="translate(ancestor::h:House/@City, $Lower, $Upper)" />/
                <xsl:value-of select="ancestor::h:House/i:Address"/>/
                <xsl:value-of select="ancestor::h:Block/@number"/>/
                <xsl:value-of select="@nuber"/>
            </Adress>
            <HouseRoomsCount>
                <xsl:value-of select="count(ancestor::h:House/descendant::r:Room)"/>
            </HouseRoomsCount>
            <BlockRoomsCount>
                <xsl:value-of select="count(ancestor::h:Block/descendant::r:Room)"/>
            </BlockRoomsCount>
       
                <xsl:call-template name="count"></xsl:call-template>
           
            <xsl:choose>
                <xsl:when test="@nuber = 1">
                    <Allocated Single="true" Double="false" Triple="false" Quarter="false"/>
                </xsl:when>
                <xsl:when test="@nuber = 2">
                    <Allocated Single="false" Double="true" Triple="false" Quarter="false"/>
                </xsl:when>
                <xsl:when test="@nuber = 3">
                    <Allocated Single="false" Double="false" Triple="true" Quarter="false"/>
                </xsl:when>
                <xsl:when test="@nuber = 4">
                    <Allocated Single="false" Double="false" Triple="false" Quarter="true"/>
                </xsl:when>
            </xsl:choose>
            
        </Room>  
        
    </xsl:template>
    
    <xsl:template name="count">
        <xsl:param name="collGuests" select="0"></xsl:param>
        <xsl:param name="numbRoom" select="1"></xsl:param>
        <xsl:variable name="collRooms" select="count(ancestor::h:House/descendant::r:Room)"/>
        <xsl:variable name="collGuest" select="ancestor::h:House/descendant::r:Room[$numbRoom]/@guests"/>
        <xsl:if test="$collRooms &gt;= $numbRoom">
            <xsl:call-template name="count">
                <xsl:with-param name="collGuests" select="$collGuests + $collGuest" />
                <xsl:with-param name="numbRoom" select="$numbRoom + 1" />
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="$collRooms &lt; $numbRoom">
            <HouseGuestsCount>
            <xsl:value-of select="$collGuests"/>
                </HouseGuestsCount>
            <GuestsPerRoomAverage>
                <xsl:value-of select="floor($collGuests div $collRooms)"/>
            </GuestsPerRoomAverage>
        </xsl:if>
        
    </xsl:template>
</xsl:stylesheet>