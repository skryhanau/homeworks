<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
    
    <xsl:template match="/">
        <Guests>
            <xsl:call-template name="string" >
                <xsl:with-param name="before" select="substring-before(Guests, '#')"/>
                <xsl:with-param name="after" select="substring-after(Guests, '#')"/>
                
            </xsl:call-template>
        </Guests>
    </xsl:template>
    
    <xsl:template name="string">
        <xsl:param name="before"/>
        <xsl:param name="after"/>
        <xsl:call-template name="else">
            <xsl:with-param name="before" select="$before"/>
        </xsl:call-template>
        <xsl:if test="substring-before($after, '#')">
            <xsl:call-template name="string">
                <xsl:with-param name="before" select="substring-before($after, '#')"/>
                <xsl:with-param name="after" select="substring-after($after, '#')"/>
            </xsl:call-template>

        </xsl:if>
    </xsl:template>
    
    <xsl:template name="else">
        <xsl:param name="before"/>
        <xsl:variable name="Type" select="substring-before($before, '|')"/>
        <xsl:variable name="AfterType" select="substring-after($before, '|')"/>
        <xsl:variable name="Age" select="substring-before($AfterType, '|')"/>
        <xsl:variable name="AfterAge" select="substring-after($AfterType, '|')"/>
        <xsl:variable name="Nationalty" select="substring-before($AfterAge, '|')"/>
        <xsl:variable name="AfterNationalty" select="substring-after($AfterAge, '|')"/>
        <xsl:variable name="Gender" select="substring-before($AfterNationalty, '|')"/>
        <xsl:variable name="AfterGender" select="substring-after($AfterNationalty, '|')"/>
        <xsl:variable name="Name" select="substring-before($AfterGender, '|')"/>
        <xsl:variable name="AfterName" select="substring-after($AfterGender, '|')"/>
        <xsl:variable name="Address" select="substring-before($AfterName, '|')"/>
        <xsl:variable name="AfterAddress" select="substring-after($AfterName, '|')"/>
        <xsl:variable name="Email" select="substring-before($AfterAddress, '|')"/>

        
        <Guest Age="{$Age}" Nationalty="{$Nationalty}" Gender="{$Gender}" Name="{$Name}">
            <Type>
                <xsl:value-of select="$Type"/>
            </Type>
            <Profile>
                <Address>
                    <xsl:value-of select="$Address"/>
                </Address>
                <xsl:if test="$Email">
                    <Email>
                        <xsl:value-of select="$Email"/>
                    </Email>
                </xsl:if>
            </Profile>
        </Guest>
    </xsl:template>
</xsl:stylesheet>